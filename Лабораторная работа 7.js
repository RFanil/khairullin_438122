/*
1. Есть массив symbols, в котором хранится алфавит (буквы и другие  символы). Есть массив encodedSymbols, в котором хранится зашифрованное сообщение.
Каждый элемент этого массива — это индекс символа из массива symbols. Программа дешифровки должна переводить элементы из массива с шифровкой (encodedSymbols)
в символы из массива алфавита (symbols) и склеивать из них расшифрованную строку. Эту строку запиши в переменную decodedMessage.
*/
var symbols = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' '];
var encodedSymbols = [12, 33, 34, 48, 50, 33, 52, 48, 50, 47, 33, 65, 66, 50, 33,34, 48, 52, 33];
var decodedMessage = '';

for (var i = 0; i < encodedMessage.length; i++) {
decodedMessage += symbols[encodedMessage[i]];
}
print(decodedMessage) //Лабораторная работа


/*
Необходимо написать программу, которая будет выполнять следующие действия. В течение тренировки человек делает несколько прыжков и собирает длины прыжков в массив attempts. 
Квалификационное eзначение хранится в переменной qualificationDistance. Программа должна выбрать три лучших прыжка, а затем посчитать среднее значение этих трёх прыжков и 
записать его в переменную averageBest. Если среднее от лучших трёх прыжков больше квалификационного значения, то человек прошёл квалификацию и переменная qualified должна 
содержать true. Если квалификация не пройдена, то в qualified должно быть false.
*/
const reducer = (accumulator, currentValue) => accumulator + currentValue;
var qualificationDistance = 59;
var attempts = [53, 12, 46, 16, 18, 74, 85, 74];
var threeBestAttempts = attempts.sort((a, b) => a < b ? 1 : a > b ? -1 : 0).slice(0, 3);
var  qualified = threeBestAttempts.reduce(reducer)/threeBestAttempts.length > qualificationDistance	//77.66666666666667 > 59 === true


/*
2. Необходимо составить программу для расчёта стоимости проекта. Имя функцию getPrice. У неё должно быть два параметра: 
время (в часах), которое нужно потратить на проект;
булево значение, которое указывает на срочность проекта — true для срочного заказа и false для обычного. Названия параметров могут быть любыми.
Для каждого проекта есть фиксированная ставка — 1500 рублей в час. Расчёт стоимости проектов выглядит так: время * ставка в час. Есть несколько нюансов. Если проект срочный, то часы уменьшаются в два раза, а ставка за час повышается в 2.5 раз.
А если время проекта больше 150 часов, ставка в час уменьшается на 250 рублей. 
В первую очередь необходимо проверить срочность. Функция должна возвращать стоимость проекта.
*/
function getPrice(time, urgency)
{
    var defaultPrice = 1500;
    
    if(urgency)
    {
        time /= 2;
        defaultPrice *=2.5;
    }
    if(time > 150)
    {
        defaultPrice -= 250;
    }
    var price = time * defaultPrice;
    return price;
}

print(getPrice(160, false)) //200 000
print(getPrice(160, true))  //300 000

/*
2. Необходимо написать программу, которая подсчитает полезность и результативность игроков на основе их статистики. Оформить код  можно в виде функции getStatistics с одним параметром — массивом игроков.
Каждый футболист в этом массиве описывается объектом с тремя полями: имя (свойство name), забитые голы (свойство goals) и голевые пасы (свойство passes).
Функция должна возвращать этот же массив, в котором каждому игроку добавлены ещё два поля: коэффициент полезности (свойство coefficient) и результативность (свойство percent).
Коэффициент полезности считается так: умножаем голы игрока на 2 и прибавляем к этому значению все голевые пасы футболиста.
Результативность (процент забитых мячей футболиста от результата всей команды) считаем так: находим сумму голов всех игроков и выясняем, сколько процентов от этого числа забил каждый футболист. Округляйте значение с помощью Math.round.
*/
class Player {
  constructor(name, goals, passes) {
    this.name = name;
    this.goals = goals;
    this.passes = passes;
  }
}

function getStatistics(players) {
    const totalGoals = players.reduce((acc, curr) => acc + curr.goals, 0);
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    return players.map(player => {
        player.coefficient = player.goals * 2 + player.passes;
        player.percent = Math.round(player.goals * 100 / totalGoals);
        return player;
    })
};

var player1 = new Player('Player1', 3, 4);
var player2 = new Player('Player2', 5, 6);
const players = [player1, player2];
var playerWithStatisctics = getStatistics(players);         /*
                                                                playerWithStatisctics
                                                                (2) [Player, Player]
                                                                0: Player
                                                                coefficient: 10
                                                                goals: 3
                                                                name: "Player1"
                                                                passes: 4
                                                                percent: 38
                                                                1: Player
                                                                coefficient: 16
                                                                goals: 5
                                                                name: "Player2"
                                                                passes: 6
                                                                percent: 63

                                                            */
